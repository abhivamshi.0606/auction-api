package com.example.auction.repositories;

import com.example.auction.models.AuctionBid;
import com.example.auction.models.AuctionItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuctionRepository extends CrudRepository<AuctionItem, Long> {
//    public List<AuctionBid> findAllByAuctionBid_Id(Integer id);

}
