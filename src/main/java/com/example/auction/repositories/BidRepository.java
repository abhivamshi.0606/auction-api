package com.example.auction.repositories;

import com.example.auction.models.AuctionBid;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Iterator;

@Repository
public interface BidRepository extends CrudRepository<AuctionBid, Long> {
    Iterable<AuctionBid> findAllByItemId(long itemId);
}
