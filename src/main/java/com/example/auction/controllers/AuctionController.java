package com.example.auction.controllers;

import com.example.auction.constants.Routes;
import com.example.auction.exception.BidOutsideAuctionHoursException;
import com.example.auction.models.AuctionBid;
import com.example.auction.models.AuctionItem;
import com.example.auction.service.AuctionService;
import com.example.auction.service.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class AuctionController {

    @Autowired
    AuctionService auctionService;

    @Autowired
    BidService bidService;

    //Get Auction Items list
    @GetMapping(value = Routes.GET_ALL_ITEMS)
    public List<AuctionItem> getInvalidTags(){
        return auctionService.getAllItems();
    }

    @GetMapping(value = Routes.GET_ITEM_INFO)
    private AuctionItem getAuctionItem(@PathVariable("itemId") long itemId)
    {
        return auctionService.getAuctionItemById(itemId);
    }

    //Set Auction timings Start and End
    @PutMapping(value = Routes.AUCTION_TIME)
    public void getAuctionTime(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime start, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime end){
        bidService.setAuctionStartTime(start);
        bidService.setAuctionEndTime(end);
    }

    @GetMapping(value = Routes.GET_BIDS)
    private List<AuctionBid> getAllAuctionBids()
    {
        return bidService.getAuctionBids();
    }

    //Post a new Bid to a given item
    @PostMapping(value = Routes.POST_BID_FOR_ITEM)
    private long saveBid(@RequestBody AuctionBid auctionBid) throws BidOutsideAuctionHoursException {
        bidService.saveOrUpdate(auctionBid);
        return auctionBid.getId();
    }

    //Get the bid/auction winner whose bid for an item is highest and the bid time is after the bid ended.
    @GetMapping(value = Routes.GET_ITEM_AUCTION_WINNER)
    private String getAuctionWinner(@PathVariable("itemId") long itemId) throws BidOutsideAuctionHoursException {
        return bidService.getAuctionWinnerById(itemId);
    }
}
