package com.example.auction.service;

import com.example.auction.models.AuctionItem;
import com.example.auction.repositories.AuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuctionService {

    @Autowired
    AuctionRepository auctionRepository;

    public List<AuctionItem> getAllItems() {
        Iterable<AuctionItem> itemsIterator = auctionRepository.findAll();
        List<AuctionItem> items = new ArrayList<>();
        itemsIterator.forEach(auctionItem -> items.add(auctionItem));
        return items;
    }

    public AuctionItem getAuctionItemById(long id) {
        return auctionRepository.findById(id).get();
    }
}
