package com.example.auction.service;

import com.example.auction.exception.BidOutsideAuctionHoursException;
import com.example.auction.models.AuctionBid;
import com.example.auction.repositories.BidRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BidService {

    @Autowired
    BidRepository bidRepository;

    private LocalDateTime auctionStartTime = LocalDateTime.now();
    private LocalDateTime auctionEndTime = LocalDateTime.now().plusMinutes(10); //Default

    public void saveOrUpdate(AuctionBid auctionBid) throws BidOutsideAuctionHoursException {
//        System.out.println("auctionBid = " + auctionBid);
        LocalDateTime now = LocalDateTime.now();
        if(now.isBefore(auctionStartTime)) {
            throw new BidOutsideAuctionHoursException("Auction not started yet");
        }
        else if(now.isAfter(auctionEndTime)) {
            throw new BidOutsideAuctionHoursException("Auction is already concluded");
        }
        bidRepository.save(auctionBid);
    }

    public List<AuctionBid> getAuctionBids() {
        List<AuctionBid> auctionBids = new ArrayList<>();
        bidRepository.findAll().forEach(auctionBid -> auctionBids.add(auctionBid));
        return auctionBids;
    }

    public String getAuctionWinnerById(long itemId) throws BidOutsideAuctionHoursException {
        LocalDateTime now = LocalDateTime.now();
        if(now.isBefore(auctionEndTime)) {
            throw new BidOutsideAuctionHoursException("Auction is still open. Please try again.");
        }
        List<AuctionBid> auctionBids = new ArrayList<>();
        bidRepository.findAllByItemId(itemId).forEach(o -> auctionBids.add(o));
        System.out.println("auctionBids = " + auctionBids);
        if(auctionBids.size() == 0) {
            return "No bids received for the auction of item "+ itemId;
        }
        String winner = auctionBids.get(0).getBidOwner();
        double maxBidValue = 0.0;
        for(AuctionBid auctionBid : auctionBids) {
            if(maxBidValue < auctionBid.getBidPrice())
            {
                maxBidValue = auctionBid.getBidPrice();
                winner = auctionBid.getBidOwner();
            }
        }
        return winner;
    }

    public void setAuctionStartTime(LocalDateTime auctionStartTime) {
        this.auctionStartTime = auctionStartTime;
    }

    public void setAuctionEndTime(LocalDateTime auctionEndTime) {
        this.auctionEndTime = auctionEndTime;
    }
}
