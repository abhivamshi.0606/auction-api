package com.example.auction.service;

import com.example.auction.models.AuctionItem;
import com.example.auction.repositories.AuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Service
public class InitializationService {
    private static final Logger logger
            = Logger.getLogger(InitializationService.class.getName());

    @Autowired
    AuctionRepository auctionRepository;

    @PostConstruct
    public void init() {
        logger.info("Loading the auction items...");
        List<AuctionItem> items = new ArrayList<>();
        items.add(new AuctionItem("Apple iPhone 12", "Apple iPhone 12 smartphone."));
        items.add(new AuctionItem("Tesla Model 3", "Tesla Model 3 Car"));
        auctionRepository.saveAll(items);
        logger.info("Loading the auction items...Done");
    }
}
