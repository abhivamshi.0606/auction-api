package com.example.auction.exception;

public class BidOutsideAuctionHoursException extends Throwable {
    String message = "";
    public BidOutsideAuctionHoursException(String auction_not_started_yet) {
        this.message = auction_not_started_yet;
    }
}
