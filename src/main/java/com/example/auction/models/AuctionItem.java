package com.example.auction.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class AuctionItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long itemId;

    @Column
    private String itemName;

    @Column
    private String itemDescription;

    @Column
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="itemId", referencedColumnName="itemId")
    private List<AuctionBid> auctionBids;

    public AuctionItem() {
    }

    public AuctionItem(String itemName, String itemDescription) {
        this.itemName = itemName;
        this.itemDescription = itemDescription;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }
}
