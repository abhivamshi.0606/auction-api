package com.example.auction.models;

import javax.persistence.*;

@Entity
@Table
public class AuctionBid {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long id;

    @Column
    private String bidOwner;

    @Override
    public String toString() {
        return "AuctionBid{" +
                "id=" + id +
                ", bidOwner='" + bidOwner + '\'' +
                ", bidPrice=" + bidPrice +
                ", itemId=" + itemId +
                '}';
    }

    @Column
    private double bidPrice;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name="itemId", nullable=false)
    @Column
    private long itemId;

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBidOwner() {
        return bidOwner;
    }

    public void setBidOwner(String bidOwner) {
        this.bidOwner = bidOwner;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }
}
