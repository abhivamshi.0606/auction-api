package com.example.auction.constants;

public class Routes {
    public static final String GET_ALL_ITEMS = "/items";
    public static final String GET_ITEM_INFO = GET_ALL_ITEMS + "/{itemId}";
    public static final String POST_BID_FOR_ITEM = "/bids";
    public static final String GET_BIDS = "/bids";
    public static final String GET_ITEM_AUCTION_WINNER = GET_ALL_ITEMS + "/{itemId}" + "/winner";
    public static final String AUCTION_TIME = "/auction";
}
