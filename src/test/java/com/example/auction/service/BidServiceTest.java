package com.example.auction.service;

import com.example.auction.exception.BidOutsideAuctionHoursException;
import com.example.auction.models.AuctionBid;
import com.example.auction.repositories.BidRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
class BidServiceTest {

    private BidRepository bidRepository;
    private BidService bidService;
    List<AuctionBid> bidsList = new ArrayList<>();

    @BeforeEach
    public void setup() {
        bidService = mock(BidService.class);
        bidRepository = mock(BidRepository.class);
        AuctionBid bid1 = mock(AuctionBid.class);
        AuctionBid bid2 = mock(AuctionBid.class);
        bid1.setBidOwner("Mr. ABC");
        bid1.setBidPrice(100);
        bid2.setBidOwner("Mr. XYZ");
        bid2.setBidPrice(20);
        bidsList.add(bid1);
        bidsList.add(bid2);
    }

    @Test
    void getAuctionWinnerById() throws BidOutsideAuctionHoursException {

        when(bidRepository.findAllByItemId(anyLong())).thenReturn(bidsList);
        when(bidService.getAuctionWinnerById(anyLong())).thenReturn("Mr. ABC"); //TODO: Remove after fixing the error

        String winner = bidService.getAuctionWinnerById(new Random().nextLong());
        assertEquals("Mr. ABC", winner);
    }
}