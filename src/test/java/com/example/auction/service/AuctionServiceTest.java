package com.example.auction.service;

import com.example.auction.models.AuctionItem;
import com.example.auction.repositories.AuctionRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
class AuctionServiceTest {
    List<AuctionItem> items = new ArrayList<>();

    AuctionService auctionService;
    AuctionRepository auctionRepository;

    AuctionItem auctionItem;

    @BeforeEach
    public void setUp() {
        auctionRepository = mock(AuctionRepository.class);
        auctionService = mock(AuctionService.class);
        items.add(new AuctionItem("Apple iPhone 12", "Apple iPhone 12 smartphone."));
        items.add(new AuctionItem("Tesla Model 3", "Tesla Model 3 Car"));
        when(auctionRepository.findAll()).thenReturn((Iterable<AuctionItem>)items);
    }

    @Test
    void getAllItems() {
        auctionService = mock(AuctionService.class);
        when(auctionService.getAllItems()).thenReturn(items);
        List<AuctionItem> list = auctionService.getAllItems();
        assertSame(list, items);
    }

    @Test
    void getAuctionItemById() {
        auctionItem = mock(AuctionItem.class);
        when(auctionService.getAuctionItemById(anyLong())).thenReturn(auctionItem);

        AuctionItem auctionItemById = auctionService.getAuctionItemById(new Random().nextLong());
        assertSame(auctionItemById, auctionItem);
    }
}